import { Inter } from "next/font/google";
import { useState, useEffect } from "react";
import Seo from "@/components/Seo";
import Header from "@/components/organisms/Header";
import Billboard from "@/components/organisms/Billboard";
import EventList from "@/components/organisms/EventList";
import { api } from "@/lib/graphql/api";
import { BILLBOARD, HOME } from "@/lib/graphql/query";
import Footer from "@/components/organisms/Footer";
import Link from "next/link";

const inter = Inter({ subsets: ["latin"] });

export async function getServerSideProps() {
  const { concerts, brands }: any = await api.request(HOME);
  const { billboard }: any = await api.request(BILLBOARD);

  return {
    props: {
      concerts,
      brands,
      billboard,
    },
  };
}

export default function Home({ concerts, brands, billboard }: any) {
  const [isModalOpen, setIsModalOpen] = useState(false);

  useEffect(() => {
    setIsModalOpen(false); // Modal muncul saat halaman diload
  }, []);

  const closeModal = () => {
    setIsModalOpen(false);
  };

  const formatDate = (dateStr: string) => {
    const date = new Date(dateStr);
    const options: any = { day: "numeric", month: "long", year: "numeric" };
    return date.toLocaleDateString("en-US", options);
  };

  return (
    <>
      <Seo
        metaTitle="PK Entertainment"
        metaDesc="PK Entertainment"
        metaKey="Event Organizer Promotor"
      />
      <Header />

      <Billboard data={billboard.concerts} />

      <div className="lg:pb-20 pb-10 lg:pt-10 pt-1">
        <EventList title="Events" data={concerts} />
      </div>

      <Footer />
    </>
  );
}
